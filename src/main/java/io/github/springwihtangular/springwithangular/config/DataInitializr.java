package io.github.springwihtangular.springwithangular.config;


import io.github.springwihtangular.springwithangular.entity.Role;
import io.github.springwihtangular.springwithangular.entity.User;
import io.github.springwihtangular.springwithangular.repository.RoleRepository;
import io.github.springwihtangular.springwithangular.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {

        List<User> users = userRepository.findAll();

        if (users.isEmpty()) {

            createUser("Nataniel", "nataniel.paiva@gmail.com", "{noop}123456", "ROLE_ADMIN");
            createUser("João","joao@gmail.com","123456", "ROLE_ADMIN");
            createUser("Maria", "maria@gmail.com","123456", "ROLE_ADMIN");

        }

    }

    public void createUser(String name, String email, String password, String roleName) {

        Role role = new Role(roleName);

        this.roleRepository.save(role);
        User user = new User(name, email, password, Arrays.asList(role));
        userRepository.save(user);
    }

}

