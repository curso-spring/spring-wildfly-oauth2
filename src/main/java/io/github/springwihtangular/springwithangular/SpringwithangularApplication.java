package io.github.springwihtangular.springwithangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Nataniel Paiva <nataniel.paiva@gmail.com>
 */

@SpringBootApplication
public class SpringwithangularApplication extends SpringBootServletInitializer {


	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringwithangularApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(getClass());
	}


}