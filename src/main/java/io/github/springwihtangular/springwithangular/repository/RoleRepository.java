package io.github.springwihtangular.springwithangular.repository;

import io.github.springwihtangular.springwithangular.entity.Role;
import io.github.springwihtangular.springwithangular.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
