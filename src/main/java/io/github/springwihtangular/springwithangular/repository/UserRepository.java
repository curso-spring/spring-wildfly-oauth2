package io.github.springwihtangular.springwithangular.repository;

import io.github.springwihtangular.springwithangular.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {


    User findByEmail(String email);
}
