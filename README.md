# Spring Boot


Spring Data
---

* Especificação JPA
* Entidades
* Interfaces Spring Data Repository
* Spring Data na prática

MVC e Criação de APIs REST
---

* MVC
* Controller
* Criação de um endpoint
* Integração do endpoint com o Repository
* Postman

Spring Security e autenticação OAuth2
---

* Spring Security
* Autenticação OAuth2
* AccessToken
* Proteger os endpoints
* Configurações produtivas no Postman
* Link do postman https://www.getpostman.com/collections/9cb2edc135b12a7ec0c9



